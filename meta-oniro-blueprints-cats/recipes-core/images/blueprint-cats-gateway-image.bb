# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT
 
require recipes-core/images/blueprint-cats-image.bb

IMAGE_INSTALL:append = " \
    libcoap-bin \
    packagegroup-thread-br \
    packagegroup-thread-client \
    tcpdump \
    networkmanager-softap-config \
    ssh \
    sshd \
 "

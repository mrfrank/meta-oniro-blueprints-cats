# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require recipes-core/images/oniro-image-base.bb

REQUIRED_DISTRO_FEATURES = "wayland"

IMAGE_INSTALL:append = " \
    cats \
    weston \
    weston-init \
"

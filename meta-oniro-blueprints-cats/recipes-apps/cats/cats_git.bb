# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-License-Identifier: MIT

SRC_URI = "git://gitlab.eclipse.org/eclipse/oniro-blueprints/context-aware-touch-screen/context-aware-touch-screen.git;protocol=https;nobranch=1;destsuffix=${S}"
SRCREV = "59ba9d77e0015d7cfeb331cafc3cbc6bcd4c0382"

SRC_URI += "file://catsd.service"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSES/Apache-2.0.txt;md5=c846ebb396f8b174b10ded4771514fcc"

DEPENDS += "lvgl lv-drivers libcoap"

SUMMARY = "Context Aware Touch Screen Demo"

REQUIRED_DISTRO_FEATURES = "wayland"

inherit pkgconfig systemd features_check

EXTRA_OEMAKE += "sysroot=${RECIPE_SYSROOT}"
EXTRA_OEMAKE += "DESTDIR=${D}"
EXTRA_OEMAKE += "lvgl_driver=wayland"

do_install() {
    install -D -m 0644 "${WORKDIR}/catsd.service" "${D}${systemd_system_unitdir}/catsd.service"
    oe_runmake install
}

SYSTEMD_SERVICE:${PN} = "catsd.service"
